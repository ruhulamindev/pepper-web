import React from "react";
import Brand from "../components/brand";
import styled, { injectGlobal } from "styled-components";

injectGlobal`
  .sidebar-opened .feedback-sidebar{
    transform: translateX(0);
  }
`;

const SidebarStyle = styled.section`
  overflow-y: auto;
  transition: 0.3s all ease;
  position: fixed;
  z-index: 9999;
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.08);
  background: white;
  top: 0;
  bottom: 0;
  left: 0;
  height: 100%;

  @media (max-width: 45em) {
    transform: translateX(-105%);
  }

  .sidebar-wrapper {
    width: 100%;
    height: 100%;
    width: 34rem;
    min-height: 600px;
    position: relative;
    padding: 1px;
  }

  .feedback-title {
    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 2.2rem;
    font-weight: bold;
    margin-left: 6rem;
    margin-top: 3rem;
    margin-bottom: 2rem;
  }

  .feedback-links {
    &__column {
      padding: 0 6rem;
      color: #0f0a30;
      font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen,
        Ubuntu, Cantarell, "Open Sans", "Helvetica Neue", sans-serif;
      font-size: 1.8rem;
      font-weight: 400;
      display: flex;
      flex-direction: column;

      a {
        padding: 2rem 0;
        transition: 0.2s all;

        &:hover {
          color: #d0011b;
        }
      }

      &:first-child {
        border-bottom: 1px solid #f6f6f6;
        margin-bottom: 5rem;
        padding-bottom: 5rem;
      }
    }
  }

  .sidebar-brand {
    position: absolute;
    bottom: 4rem;
    left: 5rem;
    margin-top: 5rem;
  }

  .sidebar-close {
    display: none;

    @media (max-width: 45em) {
      display: block;
      font-size: 4rem;
      color: #00000069;
      text-align: right;
      padding: 1rem;
    }
  }
`;

function closeSidebar() {
  document.body.classList.remove("sidebar-opened");
}

export function Sidebar() {
  return (
    <SidebarStyle className="feedback-sidebar">
      <div className="sidebar-wrapper">
        <div onClick={closeSidebar} className="sidebar-close">
          &times;
        </div>

        <div className="feedback-title">Feedback</div>

        <div className="feedback-links">
          <div className="feedback-links__column">
            <a href="#a">Home</a>
            <a href="#a">Set Offers</a>
            <a href="#a">View Purchases</a>
          </div>

          <div className="feedback-links__column">
            <a href="#a">Profile</a>
            <a href="#a">Help</a>
            <a className="color-primary" href="#a">
              Feedback
            </a>
          </div>
        </div>

        <Brand className="sidebar-brand" />
      </div>
    </SidebarStyle>
  );
}
