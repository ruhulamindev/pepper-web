import React from "react";
import styled from "styled-components";
import { RestaurantDetails } from "./RestaurantDetails";
import { Sidebar } from "./Sidebar";

const FeedbackStyle = styled.div`
  position: relative;

  .feedback-menu {
    display: none;

    @media (max-width: 53.125em) {
      display: block;
      font-size: 3rem;
      margin: 4rem;
    }
  }
`;

const Main = styled.main`
  padding: 2.7rem;
  margin-left: 34rem;
  transition: 0.3s all ease;

  @media (max-width: 45em) {
    margin: 0;
  }

  .feedback-card {
    box-shadow: 0 1px 6px rgba(0, 0, 0, 0.1);
    background-color: #ffffff;
    width: 100%;
    padding: 2rem;
    margin-top: 2rem;
    border-radius: 5px;

    &__title {
      color: #d0011b;
      font-family: "Avenir Next";
      font-size: 2rem;
      font-weight: bold;
      margin-bottom: 4rem;
    }

    &__form {
      width: 100%;
    }

    &__form-title {
      color: #767791;
      font-family: "Avenir Next";
      font-size: 1.8rem;
      font-weight: bold;
      margin-bottom: 1rem;
    }

    &__form-textarea {
      width: 100%;
      box-shadow: 0 0 1px 2px rgba(0, 0, 0, 0.1);
      border: 0;
      margin: 2rem 0;
      border-radius: 5px;
    }

    &__submit {
      box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
      border-radius: 21px;
      background-color: #d0011b;
      border: 0;
      color: #ffffff;
      font-family: "Avenir Next";
      font-size: 1.8rem;
      font-weight: bold;

      line-height: 3.1rem;
      height: 3rem;
      padding: 0 3rem;
    }
  }
`;

function openSidebar() {
  document.body.classList.add("sidebar-opened");
}

const Feedback = () => {
  return (
    <FeedbackStyle>
      <Sidebar />
      <div onClick={openSidebar} className="feedback-menu">
        &#9777;
      </div>
      <Main className="feedback-main-content">
        <RestaurantDetails />
        {/* feedback card */}
        <div className="feedback-card">
          <div className="feedback-card__title">Feedback</div>

          <form
            onSubmit={e => e.preventDefault()}
            className="feedback-card__form"
          >
            <div className="feedback-card__form-title">
              What can we do better?
            </div>
            <textarea
              name="feedback-card-textarea"
              rows="10"
              className="feedback-card__form-textarea"
            />
            <input
              type="submit"
              value="Submit"
              className="feedback-card__submit"
            />
          </form>
        </div>
      </Main>
    </FeedbackStyle>
  );
};

export default Feedback;
