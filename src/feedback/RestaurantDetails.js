import React from "react";
import styled from "styled-components";

const RestaurantDetailsWrapper = styled.div`
  display: inline-flex;
  align-items: center;

  img {
    box-shadow: 0 1px 6px rgba(0, 0, 0, 0.1);
    border-radius: 50%;
  }

  .restaurant-details {
    color: #0f0a30;
    font-family: "Avenir Next";
    font-size: 1.7rem;
    font-weight: bold;
    margin-left: 1rem;

    &__title {
      font-weight: bold;
      font-family: "Avenir Next";
    }

    &__twitter {
      color: #c1c0c9;
      font-size: 1.6rem;
      font-weight: 400;
      font-family: Helvetica;
      font-weight: 400;
    }
  }
`;

export function RestaurantDetails() {
  return (
    <RestaurantDetailsWrapper>
      <img
        src={require("../assets/img/feedback-black-rest.png")}
        alt="black restaurant"
      />
      <div className="restaurant-details">
        <div className="restaurant-details__title">Black Restaurant</div>
        <div className="restaurant-details__twitter">@blackrestau</div>
      </div>
    </RestaurantDetailsWrapper>
  );
}
