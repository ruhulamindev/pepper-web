import React from "react";
import styled from "styled-components";

import Brand from "../components/brand";
import Button from "../components/button";

const LogInStyle = styled.div`
  display: flex;
  background-color: #fcfcfc;

  @media (max-width: 39.375em) {
    flex-direction: column;
  }
`;

const Sidebar = styled.div`
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.08);
  background-color: #ffffff;
  width: 35rem;
  height: 100vh;
  text-align: center;

  @media (max-width: 39.375em) {
    flex-direction: column;
    height: auto;
    width: 100%;

    div {
      margin: 0;
      padding: 1rem;
    }
  }

  & > * {
    margin: 4rem auto;
  }
`;

const Main = styled.div`
  width: calc(100% - 35rem);
  margin: 4rem;
  padding: 4rem;
  background: white;
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.08);

  .main-content {
    /* display: flex; */
    text-align: center;
    max-width: 30rem;
  }

  @media (max-width: 39.375em) {
    width: 100%;
    margin: 0.3rem;
    margin-top: 4rem;
    padding: 2rem;
  }

  & > * {
    max-width: 30rem;
    min-width: 25rem;

    @media (max-width: 39.375em) {
      margin: 0 auto;
    }
  }

  h1 {
    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 3.5rem;
    font-weight: bold;
    margin-bottom: 4rem;
  }

  input {
    margin-bottom: 3rem;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
    border-radius: 21px;
    background-color: #e6e6e6;
    border: 0;
    height: 4.4rem;
    padding-left: 2rem;

    &,
    &::placeholder {
      color: white;
    }
  }

  button {
    margin-bottom: 0.5rem;
    width: 30rem;
  }

  .note {
    color: #0f0a30;
    font-family: "Avenir Next";
    font-size: 1.5rem;
    font-weight: 400;
    opacity: 0.8;
    margin-top: 0.5rem;
    text-decoration: underline;
  }
`;

const LogIn = () => {
  return (
    <LogInStyle>
      <Sidebar>
        <Brand />
      </Sidebar>
      <Main>
        <div className="main-content">
          <h1>Restaurant Login</h1>
          <input placeholder="Email" type="text" />
          <input placeholder="Password" type="text" />
          <Button primary>Login</Button>
          <div className="note">Signup your restaurant for free</div>
        </div>
      </Main>
    </LogInStyle>
  );
};

export default LogIn;
