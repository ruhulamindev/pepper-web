import React from "react";
import { storiesOf } from "@storybook/react";

import "./index.css";

import Homepage from "../homepage";
import Feedback from "../feedback";
import LogIn from "../login";
import SignUp from "../signup";

storiesOf("pages", module)
  .add("homepage", () => <Homepage />)
  .add("feedback", () => <Feedback />)
  .add("Login", () => <LogIn />)
  .add("SignUp", () => <SignUp />);
