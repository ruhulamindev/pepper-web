import React from "react";
import styled from "styled-components";

import Brand from "../components/brand";
import Button from "../components/button";

const SignUpStyle = styled.div`
  display: flex;
  background-color: #fcfcfc;

  @media (max-width: 66.25em) {
    flex-direction: column;
  }
`;

const Sidebar = styled.div`
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.08);
  background-color: #ffffff;
  width: 35rem;
  height: 100vh;
  text-align: center;

  @media (max-width: 66.25em) {
    flex-direction: column;
    height: auto;
    width: 100%;

    div {
      margin: 0;
      padding: 1rem;
    }
  }

  & > * {
    margin: 4rem auto;
  }
`;

const Main = styled.div`
  width: calc(100% - 35rem);
  margin: 4rem;
  padding: 4rem;
  background: white;
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.08);
  display: flex;
  flex-direction: column;
  text-align: center;

  @media (max-width: 66.25em) {
    width: 100%;
    margin: 0.3rem;
    margin-top: 4rem;
    padding: 2rem;
  }

  h1 {
    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 3.5rem;
    font-weight: bold;
    margin-bottom: 4rem;
    text-align: left;
  }

  .main-content {
    display: flex;
    overflow: hidden;
    justify-content: space-between;

    @media (max-width: 48.0625em) {
      flex-flow: column wrap;
      justify-content: center;
      align-items: center;
    }

    .input-wrapper {
      width: 35%;
      display: flex;
      flex-flow: column wrap;
      justify-content: center;
      align-items: center;

      @media (max-width: 48.0625em) {
        margin-bottom: 6rem;
        width: 100%;
      }

      & > * {
        margin-bottom: 2rem;
        min-width: 25rem;
        max-width: 30rem;
      }

      input {
        margin-bottom: 3rem;
        box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
        border-radius: 21px;
        background-color: #e6e6e6;
        border: 0;
        height: 4.4rem;
        padding-left: 2rem;

        &,&::placeholder{
          color: white;
          font-weight: bold;
        }
      }

      button{
        width: 30rem;
      }

      .note{
        font-weight: 600;
        span{
          text-decoration: underline;
        }
      }
    }

    .video-wrapper {
      width: 60%;

      @media (max-width: 48.0625em) {
        width: 100%;
      }

      .video {
        width: 100%;
        position: relative;
        padding-bottom: 56.25%;
        height: 0;

        border-radius: 1rem;
        overflow: hidden;
        box-shadow: 0px 4px 44px rgba(0, 0, 0, 0.1);
        background: #c7aa9d;

        &-title {
          color: #d0011b;
          font-family: "Avenir Next";
          font-size: 2rem;
          font-weight: bold;
          margin-top: 2rem;
        }

        iframe {
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
        }
      }
    }
  }
`;

const SignUp = () => {
  return (
    <SignUpStyle>
      <Sidebar>
        <Brand />
      </Sidebar>
      <Main>
        <h1>Sign Up Your Restaurant Here</h1>
        <div className="main-content">
          <div className="input-wrapper">
            <input placeholder="Email" type="text" />
            <input placeholder="Password" type="text" />
            <input placeholder="Restaurant Name" type="text" />
            <input placeholder="Restaurant Username" type="text" />
            <input placeholder="Restaurant Phone #" type="text" />
            <Button primary>Sign up</Button>
            <div className="note">Already registered? <span>Login Here</span></div>
          </div>

          <div className="video-wrapper">
            <div className="video">
              <iframe
                width="472px"
                height="261px"
                src="https://www.youtube.com/embed/4LMojMFIuOM"
                frameBorder="0"
                allow="autoplay; encrypted-media"
                allowFullScreen
              />
            </div>
            <div className="video-title">
              More customers, more profits with Pepper.
            </div>
          </div>
        </div>
      </Main>
    </SignUpStyle>
  );
};

export default SignUp;
