import { FreeMoney } from "./FreeMoney";

import React from "react";
import styled from "styled-components";

import { CtaSection } from "./CtaSection";
import Header from "./Header";
import MoneySection from "./moneysection";
import Brand from "../components/brand";

const Footer = styled.footer`
  .footer-company-details {
    display: flex;
    flex-direction: column;

    color: #4a494a;
    font-family: "Avenir Next";
    font-size: 2.2rem;
    font-weight: 400;

    & > * {
      margin-bottom: 2rem;
    }

    .footer__social {
      & > *:not(:first-child) {
        margin-left: 2rem;
      }
    }
  }

  .footer-legal {
  }

  .footer-download-app {
    width: 100%;
    margin: 6rem 0;
    display: flex;
    justify-content: center;

    img:first-child {
      margin-right: 2rem;
    }
  }

  .footer-legal__links {
    display: flex;
    justify-content: space-between;
    color: #4a494a;
    font-family: "Avenir Next";
    font-size: 2rem;
    font-weight: 400;
    letter-spacing: 0.03px;
    margin-bottom: 3rem;
  }
`;

const Homepage = () => {
  return (
    <React.Fragment>
      <Header />
      <div className="container">
        <CtaSection />
      </div>

      <MoneySection />
      <FreeMoney />
      <div className="container">
        <Footer>
          <div className="footer-company-details">
            <Brand />
            <div className="footer__social">
              {["facebook", "twitter", "instagram"].map(_ => (
                <a href={`#${_}`}>
                  <img src={require(`../assets/img/${_}.png`)} alt={`${_}`} />
                </a>
              ))}
            </div>
            <a href="#a">About Pepper inc.</a>
            <a href="#a">Become a RESTAURANT PARTNER</a>
            <a href="#a">FAQs</a>
            <a href="#a">Help</a>
          </div>

          <div className="footer-legal">
            <div className="footer-download-app">
              <img
                src={require("../assets/img/play-store.png")}
                alt="playstore"
                className="cta-download-btn"
              />
              <img
                src={require("../assets/img/google-play.png")}
                alt="playstore"
                className="cta-download-btn"
              />
            </div>

            <div className="footer-legal__links">
              <a href="#a">2018 Pepper Inc.</a>
              <a href="#a">Privacy</a>
              <a href="#a">Terms</a>
            </div>
          </div>
        </Footer>
      </div>
    </React.Fragment>
  );
};

export default Homepage;
