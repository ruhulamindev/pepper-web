import React from "react";
import styled from "styled-components";
import Button from "../components/button";

const HeaderStyle = styled.div`
  display: flex;
  background: black;
  padding: 2rem;
  justify-content: center;
  align-items: center;
  flex-flow: row wrap;

  .main-header__btn {
    &:first-child {
      @media (max-width: 27.687em) {
        margin-bottom: 1rem;
      }
      margin-right: 1rem;
    }
    &:last-child {
      background: white;
      color: black;
    }
  }
`;

function Header() {
  return (
    <HeaderStyle>
      <Button className="main-header__btn" primary>
        Restaurant Login
      </Button>
      <Button className="main-header__btn" secondary>
        Register Your Restaurant
      </Button>
    </HeaderStyle>
  );
}

export default Header;
