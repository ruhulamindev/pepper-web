import React from "react";

import Button from "../components/button";
import styled from "styled-components";
import ColoredHeader from "../components/coloredHeader";

const FreeMoneyStyle = styled.div`
  color: #000000;
  font-family: "Avenir Next";
  font-size: 2.2rem;
  font-weight: 400;
  margin: 10rem 0;
  line-height: 1.4;

  h2 {
    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 3.6rem;
    font-weight: bold;
    margin-bottom: 2rem;
  }

  .freemoney-row {
    display: flex;
    justify-content: space-between;

    @media (max-width: 61.9375em) {
      flex-flow: row wrap;
    }
  }

  .freemoney-column {
    max-width: 65rem;
    @media (max-width: 61.9375em) {
      margin-bottom: 4rem;
    }
  }

  .freemoney-process {
    margin-bottom: 4rem;
    max-width: 40rem;

    &__title {
      font-weight: bolder;
      margin: 1rem 0;
    }

    &__details {
    }
  }
`;

export function FreeMoney() {
  return (
    <FreeMoneyStyle>
      <ColoredHeader>Get Free Money</ColoredHeader>

      <div className="freemoney-row container">
        <div className="freemoney-column">
          There are <b>two ways</b> to help Pepper grow and make money in the
          process! Share your referal code with your friends, blogs or followers
          and make money in the easy way!
        </div>
        <div className="freemoney-column">
          <div className="freemoney-process">
            <Button primary>Get $5.00 for FREE</Button>
            <div className="freemoney-process__title">
              Invite Friends to pepper
            </div>
            <div className="freemoney-process__details">
              For every friend who signs up with your referal code, you will
              receive <b>$5.00</b> to spend on Pepper.
            </div>
          </div>

          <div className="freemoney-process">
            <Button primary>Get $100 for FREE</Button>
            <div className="freemoney-process__title">
              Invite Businesses to Pepper
            </div>
            <div className="freemoney-process__details">
              For every business that signs up with your referal code, you will
              receive <b>$100</b> to spend on Pepper.
            </div>
          </div>
        </div>
      </div>
    </FreeMoneyStyle>
  );
}
