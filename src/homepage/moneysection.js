import React from "react";
import styled from "styled-components";
import ColoredHeader from "../components/coloredHeader";

const MoneySectionStyle = styled.div`
  margin-top: 8rem;
  margin-bottom: 8rem;
  align-items: center;

  h2 {
    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 3.6rem;
    font-weight: bold;
    margin-bottom: 2rem;
  }

  .moneysection {
    display: flex;
    justify-content: space-between;
    align-items: center;

    @media (max-width: 43.125em) {
      flex-direction: column;
    }
  }

  .pepper-advantage {
    color: #000000;
    font-family: "Avenir Next";
    font-size: 2.2rem;
    font-weight: 400;
    max-width: 90rem;
    width: 40%;
    @media (max-width: 48.062em) {
      margin-bottom: 6rem;
      width: 45%;
    }
    @media (max-width: 43.125em) {
      width: 100%;
    }
  }

  .pepper-video-wrapper {
    width: 47%;
    @media (max-width: 48.062em) {
      width: 52%;
    }
    @media (max-width: 43.125em) {
      width: 100%;
      padding-bottom: 58.25%;
    }
    position: relative;
    padding-bottom: 26.25%;
    height: 0;

    border-radius: 1rem;
    overflow: hidden;
    box-shadow: 0px 4px 44px rgba(0, 0, 0, 0.1);
    background: #c7aa9d;

    iframe {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
    }
  }
`;

const MoneySection = () => {
  return (
    <MoneySectionStyle>
      <ColoredHeader>How Pepper Works</ColoredHeader>

      <div className="moneysection container">
        <div className="pepper-advantage">
          Pepper makes getting more at your favourite restaurants, bars or clubs
          super easy! <br /> <br /> Download the app or above or watch our video
          to find out how.
        </div>
        <div className="pepper-video-wrapper">
          <iframe
            className="pepper-video"
            title="how pepper works video"
            src="https://www.youtube.com/embed/sTjBt4LsvoE"
            frameBorder="0"
            allow="autoplay; encrypted-media"
            allowFullScreen
          />
        </div>
      </div>
    </MoneySectionStyle>
  );
};

export default MoneySection;
