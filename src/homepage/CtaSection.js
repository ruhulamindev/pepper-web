import React from "react";
import styled from "styled-components";
import Brand from "../components/brand";

const CtaSectionStyle = styled.div`
  margin-top: 6rem;
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media (max-width: 48.0625em) {
    flex-flow: column-reverse;
    justify-content: center;

    .cta-intro {
      margin-top: 6rem;
    }
  }

  .cta-intro {
    max-width: 40rem;

    &__brand {
      font-size: 7rem;
      padding-bottom: 4rem;
    }

    p {
      color: #d0011b;
      font-family: "Avenir Next";
      font-size: 3.8rem;
      font-weight: bold;
      margin-bottom: 3rem;
    }

    .cta-app-stores {
      display: flex;
      justify-content: space-between;

      img {
        @media (max-width: 31.25em) {
          width: 50%;
        }
      }

      img:first-child {
        margin-right: 1rem;
      }
    }
  }
`;

export function CtaSection() {
  return (
    <CtaSectionStyle>
      <div className="cta-intro">
        <Brand className="cta-intro__brand" />
        <p>
          The easiest way to get more at Toronto's top restaurants, bars and
          clubs.
        </p>
        <br />
        <p>...Don't miss out.</p>

        <div className="cta-app-stores">
          <img
            src={require("../assets/img/play-store.png")}
            alt="playstore"
            className="cta-download-btn"
          />
          <img
            src={require("../assets/img/google-play.png")}
            alt="playstore"
            className="cta-download-btn"
          />
        </div>
      </div>

      <img
        src={require("../assets/img/pepper-photo.png")}
        alt="pepper app homepage"
        className="cta-photo"
      />
    </CtaSectionStyle>
  );
}
