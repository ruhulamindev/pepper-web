import React from "react";
import styled from "styled-components";

const PepperLogo = styled.div`
  color: #d0011b;
  font-family: "Avenir Next";
  font-size: 4.6rem;
  font-weight: bold;
`;

const Brand = props => {
  return <PepperLogo {...props}>pepper</PepperLogo>;
};

export default Brand;
