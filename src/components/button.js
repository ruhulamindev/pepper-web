import styled from "styled-components";

const Button = styled.button`
  background: ${props => (props.primary ? "#d0011b" : "#4a494a")};
  color: #ffffff;
  font-family: "Avenir Next";
  font-weight: bold;
  border-radius: 50px;
  font-size: 2rem;
  padding: 0 2.5rem;
  height: 4.2rem;
  display: inline-block;
  border: 0;
  line-height: ${props =>
    props.height ? `${props.height + 0.3}rem` : `${4.2 + 0.3}rem`};
`;

export default Button;
