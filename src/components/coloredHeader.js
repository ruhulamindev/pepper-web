import React from "react";
import styled from "styled-components";

const Header = styled.div`
  background-color: #d0011b;
  display: inline-block;
  color: white;
  font-weight: bold;
  margin-bottom: 6rem;
  padding: 3rem;
  padding-right: 7rem;
  font-size: 6rem;
  border-top-right-radius: 100px;
  border-bottom-right-radius: 100px;

  @media (max-width: 48.062em){
    font-size: 4rem;
    padding-right: 4rem;
    padding: 2rem;
  }
`;

const ColoredHeader = props => {
  return <Header>{props.children}</Header>;
};

export default ColoredHeader;
